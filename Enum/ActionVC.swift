//
//  ActionVC.swift
//  Enum
//
//  Created by uros.rupar on 5/27/21.
//

import UIKit

class ActionVC: UIViewController ,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    
    var movies:[Movie] = [
        Movie(title: "The Queen’s Gambit", poster: "gambit", rated: 9, numberOfSeason:1, genre: Genre.DRAMA, director: "Scott Frank", writer: "Scott Frank", actor: "Anya Taylor-Joy", language:Language.ENGLISH, country: "SAD", production:"Netflix"),
        
        Movie(title: "Emily in Paris", poster: "emily", rated: 9, numberOfSeason:5, genre: Genre.DRAMA, director: "Jackob Jonathan", writer: "Scott Frank", actor: "Lily Collins", language:Language.ENGLISH, country: "SAD", production:"Netflix"),
        Movie(title: "Lupin", poster: "lupin", rated: 8, numberOfSeason: 2, genre: Genre.TRILER, director: "John John", writer: "John John", actor: "Omar See", language:Language.FRENCH, country:"France", production: "Netflix"),
        Movie(title: "Pickey Blinders", poster: "picky", rated: 8, numberOfSeason: 6, genre: Genre.ACTION, director: "John John", writer: "John John", actor: "Cillian Murphy    ", language:Language.ENGLISH, country:"UK", production: "Netflix"),
        Movie(title: "Breaking Bed", poster: "bed", rated: 8, numberOfSeason: 6, genre: Genre.ACTION, director: "John John", writer: "John John", actor: "Cillian Murphy    ", language:Language.ENGLISH, country:"UK", production: "Netflix"),
        Movie(title: "Game of thrones", poster: "game", rated: 8, numberOfSeason: 6, genre: Genre.ACTION, director: "John John", writer: "John John", actor: "Cillian Murphy    ", language:Language.ENGLISH, country:"UK", production: "Netflix"),
        Movie(title: "Dark", poster: "dark", rated: 8, numberOfSeason: 6, genre: Genre.ACTION, director: "John John", writer: "John John", actor: "Cillian Murphy    ", language:Language.ENGLISH, country:"UK", production: "Netflix"),
        Movie(title: "Dark", poster: "dark", rated: 8, numberOfSeason: 6, genre: Genre.ACTION, director: "CAsa de Papel", writer: "casa", actor: "Cillian Murphy    ", language:Language.ENGLISH, country:"UK", production: "Netflix")
    ]

    override func viewDidLoad() {
        super.viewDidLoad()

        movies.reverse()
        
    }
    

   
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? ActionCell
        
        print(movies[indexPath.row].poster)
       
        cell?.posterImage.image = UIImage(named: movies[indexPath.row].poster)
        
        cell?.titleLabel.text = movies[indexPath.row].title
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
      {
        let width  = (view.frame.width-60)/2
         return CGSize(width: width, height: width)
      }
}
