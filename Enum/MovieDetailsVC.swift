//
//  MovieDetailsVC.swift
//  Enum
//
//  Created by uros.rupar on 5/27/21.
//

import UIKit

class MovieDetailsVC: UIViewController {

    var currentMovie = Movie(title: "", poster: "", rated: 0, numberOfSeason: 0, genre: Genre.ACTION, director:"" , writer: "", actor: "", language: Language.ENGLISH, country: "", production: "")
    
    
    @IBOutlet weak var posterImage: UIImageView!
    
    
    
    @IBOutlet weak var numberOfSeasonsLabel: UILabel!
    
    @IBOutlet weak var genreLabel: UILabel!
    
    @IBOutlet weak var director: UILabel!
    
    @IBOutlet weak var writerlabel: UILabel!
    
    @IBOutlet weak var actorLabel: UILabel!
    
    @IBOutlet weak var languageLabel: UILabel!
    
    @IBOutlet weak var countrylabel: UILabel!
    @IBOutlet weak var production: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        posterImage.image = UIImage(named: currentMovie.poster)
        numberOfSeasonsLabel.text = "Broj sezona: " +
        String(currentMovie.numberOfSeason)
        genreLabel.text = String(describing: currentMovie.genre)
        director.text = currentMovie.director
        writerlabel.text = currentMovie.writer
        actorLabel.text = currentMovie.actor
        countrylabel.text = currentMovie.country
        languageLabel.text = String(describing: currentMovie.language)
        production.text = currentMovie.production
        
        
    }
    
    

    

}
