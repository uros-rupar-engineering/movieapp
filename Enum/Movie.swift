//
//  Movie.swift
//  Enum
//
//  Created by uros.rupar on 5/27/21.
//

import Foundation

enum Genre {
    case ADVENTURRE,ROMANCE,DRAMA,HOROR,COMEDY,TRILER,ANIMATED,ACTION
}
enum Language {
    case ENGLISH,GERMAN,SPANISH,SERBIAN,FRENCH
}

struct Movie {
    var title: String
        
    var poster: String
    
    var rated: Int
    
    var numberOfSeason : Int
    
    let genre: Genre
    
    let director : String
    
    let writer : String
    
    let actor : String
    
    let language:Language
    
    let country : String
    
    let production : String
}

func findGenre(_ genre: String) ->Genre{
    switch genre {
    case "Adventure":
        return Genre.ADVENTURRE
    case "Romance":
        return Genre.ROMANCE
    case "Drama":
        return Genre.DRAMA
    case "Horor":
        return Genre.HOROR
    case "Comedy":
        return Genre.COMEDY
    case "Triler":
        return Genre.TRILER
    case "Animated":
        return Genre.ANIMATED
    case "Action":
        return Genre.ACTION
        
    default:
        return Genre.ACTION
    }
}

func findLanguage(_ language:String) -> Language{
    switch language {
    case "English":
        return Language.ENGLISH
    case "German":
        return Language.FRENCH
    case "Serbian":
        return Language.SERBIAN
    case "Spanish":
        return Language.SPANISH
        
        
    default:
        return Language.SPANISH
    }
}
