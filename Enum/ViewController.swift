//
//  ViewController.swift
//  Enum
//
//  Created by uros.rupar on 5/26/21.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UIPickerViewDelegate,UIPickerViewDataSource {

    let fileName = "textFile"
    let fileXstension = "csv"
    
    var fileContent : String?
    
    var movies:[Movie] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        posterImage.image = UIImage(named: commingSonMovies[0].poster)
        parseToModel()
    }
    
   
 /*   var movies:[Movie] = [
        Movie(title: "The Queen’s Gambit", poster: "gambit", rated: 9, numberOfSeason:1, genre: Genre.DRAMA, director: "Scott Frank", writer: "Scott Frank", actor: "Anya Taylor-Joy", language:Language.ENGLISH, country: "SAD", production:"Netflix"),
        
        Movie(title: "Emily in Paris", poster: "emily", rated: 9, numberOfSeason:5, genre: Genre.DRAMA, director: "Jackob Jonathan", writer: "Scott Frank", actor: "Lily Collins", language:Language.ENGLISH, country: "SAD", production:"Netflix")
    ]*/
    
    var commingSonMovies:[Movie] = [
        Movie(title: "Lupin", poster: "lupin", rated: 8, numberOfSeason: 2, genre: Genre.TRILER, director: "John John", writer: "John John", actor: "Omar See", language:Language.FRENCH, country:"France", production: "Netflix"),
        Movie(title: "Pickey Blinders", poster: "picky", rated: 8, numberOfSeason: 6, genre: Genre.ACTION, director: "John John", writer: "John John", actor: "Cillian Murphy    ", language:Language.ENGLISH, country:"UK", production: "Netflix")
    ]
    
    //Table View
    
    @IBOutlet weak var movieTable: UITableView!
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        movies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        cell.textLabel?.text = "\(indexPath.row + 1) \(movies[indexPath.row].title)" 
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "MovieDetails"{
            let controller = segue.destination as! MovieDetailsVC
            controller.currentMovie = movies[self.movieTable.indexPathForSelectedRow!.row]
            segue.destination.title =  movies[self.movieTable.indexPathForSelectedRow!.row].title
        }
    }
    
    
    //Picker View
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return commingSonMovies.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return commingSonMovies[row].title
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        posterImage.image = UIImage(named: commingSonMovies[row].poster)
    }
    
    @IBOutlet weak var posterImage: UIImageView!
    

    
    func parseToModel(){
        
        let path = Bundle.main.path(forResource: fileName, ofType: fileXstension)
        
        fileContent = try? String(contentsOfFile: path!, encoding: String.Encoding.utf8)
        if let fileContent = self.fileContent{
            
           
            
            let lineByLineArray = fileContent.split(separator: "\n")
            
            movies = []
            for row in lineByLineArray{
                let lineToArray = row.split(separator: ",")
                
                
                let title = String(lineToArray[0])
                
                let poster = String(lineToArray[1])
                let rated = Int(lineToArray[2]) ?? 0
                let numberOfseasons = Int(lineToArray[3]) ?? 0
                let genre = findGenre(String(lineToArray[4]))
                let director = String(lineToArray[5])
                let writer = String(lineToArray[6])
                let actor = String(lineToArray[7])
                let language = findLanguage(String(lineToArray[8]))
                let country = String(lineToArray[9])
                let production = String(lineToArray[10])
               
                let movie:Movie = Movie(title: title, poster: poster, rated: rated, numberOfSeason: numberOfseasons, genre: genre, director: director, writer: writer, actor: actor, language: language, country: country, production: production)
               
                                         
                
                movies.append(movie)
            }
        }
    }
    
    
    
  

}

