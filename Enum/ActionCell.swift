//
//  ActionCell.swift
//  Enum
//
//  Created by uros.rupar on 5/27/21.
//

import UIKit

class ActionCell: UICollectionViewCell {
    
    @IBOutlet weak var posterImage: UIImageView!
    
    
    @IBOutlet weak var titleLabel: UILabel!
}
